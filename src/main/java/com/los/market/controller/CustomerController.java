package com.los.market.controller;

import com.los.market.model.Customer;
import com.los.market.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;
import java.util.Locale;

/**
 * Created by Jenson Harvey on 07.10.2015.
 */

@Controller
public class CustomerController {
    @Autowired
    CustomerService serviceC;

    @Autowired
    MessageSource messageSource;

    @RequestMapping(value = { "/CustomerList" }, method = RequestMethod.GET)
    public String listCustomers(ModelMap model) {

        List<Customer> customers = serviceC.getAll();
        model.addAttribute("customers", customers);
        return "allcustomers";
    }

    @RequestMapping(value = { "/newCustomer" }, method = RequestMethod.GET)
	public String newCustomer(ModelMap model) {
		Customer customer = new Customer();
		model.addAttribute("customer", customer);
		model.addAttribute("edit", false);
		return "customerregistration";
	}

    @RequestMapping(value = { "/newCustomer" }, method = RequestMethod.POST)
	public String saveCustomer(@Valid Customer customer, BindingResult result, ModelMap model) {

		if (result.hasErrors()) {
			return "customerregistration";
		}
		if(!serviceC.isCustomerUnique(customer.getLogin(), customer.getId())){
			FieldError nameError =new FieldError("customer","name",messageSource.getMessage("non.unique.name", new String[]
					{customer.getLastName()}, Locale.getDefault()));
			result.addError(nameError);
			return "customerregistration";
		}

		serviceC.saveCustomer(customer);

		model.addAttribute("success", "Customer " + customer.getLogin() + " registered successfully");
		return "customersuccess";
	}

    @RequestMapping(value = { "/edit-{id}-customer" }, method = RequestMethod.GET)
    public String editCustomer(@PathVariable Integer id, ModelMap model) {
        Customer customer = serviceC.findById(id);
        model.addAttribute("customer", customer);
        model.addAttribute("edit", true);
        return "customerregistration";
    }

    @RequestMapping(value = { "/edit-{id}-customer" }, method = RequestMethod.POST)
    public String updateCustomer(@Valid Customer customer, BindingResult result, ModelMap model, @PathVariable Integer id) {

        if (result.hasErrors()) {
            System.out.println("ERROR");
            return "customerregistration";
        }

        if(!serviceC.isCustomerUnique(customer.getLogin(), customer.getId())){
            FieldError nameError =new FieldError("customer","name",messageSource.getMessage("non.unique.name", new String[]
                    {customer.getLastName()}, Locale.getDefault()));
            result.addError(nameError);
            return "customerregistration";
        }

        serviceC.updateCustomer(customer);

        model.addAttribute("success", "Customer " + customer.getLogin() + " updated successfully");
        return "customersuccess";
    }

    @RequestMapping(value = { "/delete-{id}-customer" }, method = RequestMethod.GET)
	public String deleteCustomer(@PathVariable Integer id) {
		serviceC.deleteCustomerById(id);
		return "redirect:/CustomerList";
	}
}
