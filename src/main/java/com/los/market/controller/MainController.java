package com.los.market.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Jenson Harvey on 07.10.2015.
 */

@Controller
@RequestMapping("/")
public class MainController {

    @RequestMapping(value = {"/", "home"}, method = RequestMethod.GET)
    public String getHellopage(ModelMap model){
        return "home";
    }
}
