package com.los.market.controller;

import com.los.market.model.Product;
import com.los.market.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;
import java.util.Locale;

/**
 * Created by Jenson Harvey on 08.10.2015.
 */

@Controller
public class ProductController {
    @Autowired
    ProductService serviceP;

    @Autowired
    MessageSource messageSource;

    @RequestMapping(value = { "/ProductList" }, method = RequestMethod.GET)
    public String listProducts(ModelMap model) {

        List<Product> products = serviceP.getAll();
        model.addAttribute("products", products);
        return "allproducts";
    }

    @RequestMapping(value = { "/newProduct" }, method = RequestMethod.GET)
    public String newProduct(ModelMap model) {
        Product product = new Product();
        model.addAttribute("product", product);
        model.addAttribute("edit", false);
        return "productregistration";
    }

    @RequestMapping(value = { "/newProduct" }, method = RequestMethod.POST)
    public String saveCustomer(@Valid Product product, BindingResult result, ModelMap model) {

        /*if (result.hasErrors()) {
            return "productregistration";
        }
        if(!serviceP.isProductUnique(product.getName(), product.getId())){
            FieldError nameError =new FieldError("product","name",messageSource.getMessage("non.unique.name", new String[]
                    {product.getName()}, Locale.getDefault()));
            result.addError(nameError);
            return "productregistration";
        }*/

        serviceP.saveProduct(product);

        model.addAttribute("success", "Product " + product.getName() +  " registered successfully");
        return "productsuccess";
    }
    @RequestMapping(value = { "/edit-{id}-product" }, method = RequestMethod.GET)
    public String editProduct(@PathVariable Integer id, ModelMap model) {
        Product product = serviceP.getById(id);
        model.addAttribute("product", product);
        model.addAttribute("edit", true);
        return "productregistration";
    }
    @RequestMapping(value = { "/edit-{id}-product" }, method = RequestMethod.POST)
    public String updateProduct(@Valid Product product, BindingResult result, ModelMap model, @PathVariable Integer id) {

        if (result.hasErrors()) {
            model.addAttribute("Fail", "Product " + product.getName() + " was not updated");
            return "productsuccess";
        }

        if(!serviceP.isProductUnique(product.getName(), product.getId())){
            FieldError nameError =new FieldError("product","name",messageSource.getMessage("non.unique.name", new String[]
                    {product.getName()}, Locale.getDefault()));
            result.addError(nameError);
            return "productregistration";
        }

        serviceP.updateProduct(product);

        model.addAttribute("success", "Product " + product.getName() + " updated successfully");
        return "productsuccess";
    }
    @RequestMapping(value = { "/delete-{id}-product" }, method = RequestMethod.GET)
    public String deleteProduct(@PathVariable Integer id) {
        serviceP.deleteProductById(id);
        return "redirect:/ProductList";
    }

}
