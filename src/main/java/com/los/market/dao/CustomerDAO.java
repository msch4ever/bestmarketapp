package com.los.market.dao;

import com.los.market.model.Customer;

import java.util.List;

/**
 * Created by Jenson Harvey on 06.10.2015.
 */

public interface CustomerDAO{

    void saveCustomer(Customer customer);
    void deleteCustomerById(Integer id);
    List<Customer> getAll();
    Customer getByLogin(final String login);
    Customer findById(Integer id);
    //Set<Customer> getAllSortedByLastName();
    //Set<Customer> getAllSortedByInvoice();
   // Customer getByLoginAndPassword(final String login, final String password);
   // Customer getByLogin(final String login);
   // Customer getByCardNumber(final String cardNumber);
    //boolean updateAll();
}
