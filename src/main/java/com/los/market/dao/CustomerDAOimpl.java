package com.los.market.dao;

import com.los.market.model.Customer;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Jenson Harvey on 06.10.2015.
 */

@Repository ("customerDao")
public class CustomerDAOimpl extends AbstractDao<Integer, Customer> implements CustomerDAO {

    public void saveCustomer(Customer customer) {
        persist(customer);
    }

    public void deleteCustomerById(Integer id) {
        Query query = getSession().createSQLQuery("delete from CUSTOMERS where customer_id = :id");
        query.setInteger("id", id);
        query.executeUpdate();
    }

    @SuppressWarnings("unchecked")
    public List<Customer> getAll() {
        Criteria criteria = createEntityCriteria();
        return (List<Customer>) criteria.list();
    }

    public Customer getByLogin(String login) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("login", login));
        return (Customer) criteria.uniqueResult();
    }

    public Customer findById(Integer id) {
        return getByKey(id);
    }

}
