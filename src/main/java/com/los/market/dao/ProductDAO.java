package com.los.market.dao;

import com.los.market.model.Product;

import java.util.List;

/**
 * Created by Jenson Harvey on 06.10.2015.
 */

public interface ProductDAO {

    void saveProduct(Product product);
    void deleteProductById(final Integer id);
    Product getById(Integer id);
    List<Product> getAll();
}
