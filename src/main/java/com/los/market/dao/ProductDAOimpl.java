package com.los.market.dao;

import com.los.market.model.Product;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Jenson Harvey on 08.10.2015.
 */

@Repository ("productDao")
public class ProductDAOimpl extends AbstractDao<Integer, Product> implements ProductDAO {

    public void saveProduct(Product product) {
        persist(product);
    }

    public void deleteProductById(Integer id) {
        Query query = getSession().createSQLQuery("delete from Products where product_id = :id");
        query.setInteger("id", id);
        query.executeUpdate();
    }

    public Product getById(Integer id) {
        return getByKey(id);
    }
    @SuppressWarnings("unchecked")
    public List<Product> getAll() {
        Criteria criteria = createEntityCriteria();
        return (List<Product>) criteria.list();
    }
}
