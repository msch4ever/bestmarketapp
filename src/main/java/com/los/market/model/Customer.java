package com.los.market.model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jenson Harvey on 06.10.2015.
 */

@Entity
@Table(name = "CUSTOMERS")
public class Customer implements Serializable {

    //static final MyLogger LOGGER = new MyLogger(Customer.class);
    private Integer id;
    private String firstName;
    private String lastName;
    private String login;
    private String password;

    private List<Product> basket;

    public Customer(){
        this.basket = new ArrayList<Product>();
    }

    public Customer(Integer id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }
    @Id
    @GeneratedValue
    @Column (name = "customer_id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column (name = "first_name")
    @Size(min=3, max=50)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column (name = "last_name")
    @Size(min=3, max=50)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void addProductToBasket(Product product){
        this.basket.add(product);
    }
   /* @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "customer_product",
            joinColumns = @JoinColumn(name = "customer_id"),
            inverseJoinColumns = @JoinColumn(name = "product_id"))
    public List<Product> getBasket(){
        return basket;
    }*/

    public void setBasket(List<Product> products){
        this.basket=products;
    }

    @Column(name = "customer_password")
    @Size(min=4)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    @Column(name = "customer_login")
    @Size(min=3)
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Customer)) return false;

        Customer customer = (Customer) o;

        if (!getId().equals(customer.getId())) return false;
        if (getFirstName() != null ? !getFirstName().equals(customer.getFirstName()) : customer.getFirstName() != null)
            return false;
        if (getLastName() != null ? !getLastName().equals(customer.getLastName()) : customer.getLastName() != null)
            return false;
        return !(getLogin() != null ? !getLogin().equals(customer.getLogin()) : customer.getLogin() != null);

    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getFirstName() != null ? getFirstName().hashCode() : 0);
        result = 31 * result + (getLastName() != null ? getLastName().hashCode() : 0);
        result = 31 * result + (getLogin() != null ? getLogin().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format("ID - %d, name - %s %s", id, firstName, lastName);
    }
}