package com.los.market.service;

import com.los.market.model.Customer;

import java.util.List;

/**
 * Created by Jenson Harvey on 06.10.2015.
 */
public interface CustomerService {

    void saveCustomer(Customer customer);
    void deleteCustomerById(Integer id);
    List<Customer> getAll();
    Customer getByLogin(final String login);
    Customer findById(Integer id);
    void updateCustomer(Customer customer);
    boolean isCustomerUnique(final String login, Integer id);
}
