package com.los.market.service;

import com.los.market.dao.CustomerDAO;
import com.los.market.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Jenson Harvey on 06.10.2015.
 */

@Service("customerService")
@Transactional
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerDAO customerDAO;

    public void saveCustomer(Customer customer) {
        customerDAO.saveCustomer(customer);
    }

    public void deleteCustomerById(Integer id) {
        customerDAO.deleteCustomerById(id);
    }

    public List<Customer> getAll() {
        return customerDAO.getAll();
    }

    public Customer getByLogin(String login) {
        return customerDAO.getByLogin(login);
    }

    public Customer findById(Integer id) {
        return customerDAO.findById(id);
    }

    public void updateCustomer(Customer customer) {
        Customer entity = customerDAO.findById(customer.getId());
        if(entity!=null){
            entity.setFirstName(customer.getFirstName());
            entity.setLastName(customer.getLastName());
            entity.setLogin(customer.getLogin());
        }
    }

    public boolean isCustomerUnique(String login, Integer id) {
        Customer customer = getByLogin(login);
        return (customer == null || ((id != null) && (customer.getId() == id)));
    }
}