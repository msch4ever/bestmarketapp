package com.los.market.service;

import com.los.market.model.Product;

import java.util.List;

/**
 * Created by Jenson Harvey on 08.10.2015.
 */
public interface ProductService {

    void saveProduct(Product product);
    void deleteProductById(Integer id);
    Product getById(Integer id);
    List<Product> getAll();
    void updateProduct(Product product);
    boolean isProductUnique(String name, Integer id);

}
