package com.los.market.service;

import com.los.market.dao.ProductDAO;
import com.los.market.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Jenson Harvey on 08.10.2015.
 */

@Service ("productService")
@Transactional
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductDAO productDAO;

    public void saveProduct(Product product) {
        productDAO.saveProduct(product);
    }

    public void deleteProductById(Integer id) {
        productDAO.deleteProductById(id);
    }

    public Product getById(Integer id) {
        Product product = productDAO.getById(id);
        return product;
    }

    public List<Product> getAll() {
        List<Product> products = productDAO.getAll();
        return products;
    }

    public void updateProduct(Product product) {
        Product entity = productDAO.getById(product.getId());
        if(entity!=null){
            entity.setName(product.getName());
            entity.setPrice(product.getPrice());
        }
    }

    public boolean isProductUnique(String name, Integer id) {
        List<Product> list = productDAO.getAll();
        boolean result = true;
        for(Product p : list){
            if(p.getId().equals(id)){
                result = false;
            }
        }
        return result;
    }
}
