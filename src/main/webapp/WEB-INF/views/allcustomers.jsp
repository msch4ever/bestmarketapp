<%--
  Created by IntelliJ IDEA.
  User: Jenson Harvey
  Date: 07.10.2015
  Time: 18:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
          pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title>Market</title>

  <style>
    tr:first-child{
      font-weight: bold;
      background-color: #C6C9C4;
    }
  </style>

</head>


<body>
<h2>List of Customers</h2>
<table>
  <tr>
    <td>Id</td><td>First Name</td><td>Last Name</td><td>Login</td><td></td><td></td>
  </tr>
  <c:forEach items="${customers}" var="customer">
    <tr>
      <td>${customer.id}</td>
      <td>${customer.firstName}</td>
      <td>${customer.lastName}</td>
      <td>${customer.login}</td>
      <td><a href="<c:url value='/edit-${customer.id}-customer' />">edit</a></td>
      <td><a href="<c:url value='/delete-${customer.id}-customer' />">delete</a></td>
    </tr>
  </c:forEach>
</table>
<br/>
<a href="<c:url value='/newCustomer' />">Add New Customer</a></br>
<a href="<c:url value='/home' />">Return Home</a></br>
</body>
</html>
