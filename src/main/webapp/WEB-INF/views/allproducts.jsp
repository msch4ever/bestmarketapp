<%--
  Created by IntelliJ IDEA.
  User: Jenson Harvey
  Date: 08.10.2015
  Time: 15:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title>Market</title>

  <style>
    tr:first-child{
      font-weight: bold;
      background-color: #C6C9C4;
    }
  </style>

</head>


<body>
<h2>List of Products</h2>
<table>
  <tr>
    <td>ID</td><td>Name</td>Price<td>Id</td><td></td>
  </tr>
  <c:forEach items="${products}" var="product">
    <tr>
      <td>${product.id}</td>
      <td>${product.name}</td>
      <td>${product.price}</td>
      <td><a href="<c:url value='/edit-${product.id}-product' />">edit</a></td>
      <td><a href="<c:url value='/delete-${product.id}-product' />">delete</a></td>
    </tr>
  </c:forEach>
</table>
<br/>
<a href="<c:url value='/newProduct' />">Add New Product</a></br>
<a href="<c:url value='/home' />">Return Home</a></br>
</body>
</html>
