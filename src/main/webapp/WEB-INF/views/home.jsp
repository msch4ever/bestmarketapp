<%--
  Created by IntelliJ IDEA.
  User: Jenson Harvey
  Date: 07.10.2015
  Time: 21:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:message code="hello.greetings" var="hello"/>

<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title>Hello</title>
</head>
<br>
<b>${hello}</b>
<br/>
<br/>
Register new custommer <a href="<c:url value='/CustomerList' />">List of All Customers</a></br>
Register new product <a href="<c:url value='/ProductList' />">List of All Products</a></br>

</body>

</html>