<%--
  Created by IntelliJ IDEA.
  User: Jenson Harvey
  Date: 07.10.2015
  Time: 19:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title>Registration Confirmation Page</title>
</head>
<body>
message : ${success}
<br/>
<br/>
Go back to <a href="<c:url value='/ProductList' />">List of All Products</a></br>
<a href="<c:url value='/home' />">Return Home</a></br>

</body>

</html>
