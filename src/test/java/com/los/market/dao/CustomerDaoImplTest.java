package com.los.market.dao;

import com.los.market.model.Customer;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;


public class CustomerDaoImplTest extends EntityDaoImplTest{

	@Autowired
	CustomerDAO customerDao;

	@Override
	protected IDataSet getDataSet() throws Exception{
		IDataSet dataSet = new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Customer.xml"));
		return dataSet;
	}

	@Test
	public void findById(){
		Assert.assertNotNull(customerDao.findById(1));
		Assert.assertNull(customerDao.findById(3));
	}

	
	@Test
	public void saveCustomer(){
		customerDao.saveCustomer(getSampleCustomer());
		Assert.assertEquals(customerDao.getAll().size(), 3);
	}
	
	@Test
	public void deleteCustomerById(){
		customerDao.deleteCustomerById(1);
		Assert.assertEquals(customerDao.getAll().size(), 1);
	}
	
	@Test
	public void deleteCustomerByInvalidId(){
		customerDao.deleteCustomerById(99999);
		Assert.assertEquals(customerDao.getAll().size(), 2);
	}

	@Test
	public void getAll(){
		Assert.assertEquals(customerDao.getAll().size(), 2);
	}
	

	public Customer getSampleCustomer(){
		Customer customer = new Customer();
		customer.setFirstName("Cass");
		customer.setLastName("Los");
		customer.setLogin("Cass");
		customer.setPassword("ssss");
		customer.setId(1);
		return customer;
	}

}
