package com.los.market.service;

import com.los.market.dao.CustomerDAO;
import com.los.market.model.Customer;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;

public class CustomerServiceImplTest {

	@Mock
	CustomerDAO dao;
	
	@InjectMocks
	CustomerServiceImpl customerService;
	
	@Spy
	List<Customer> customers = new ArrayList<Customer>();
	
	@BeforeClass
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		customers = getCustomerList();
	}

	@Test
	public void findById(){
		Customer emp = customers.get(0);
		when(dao.findById(anyInt())).thenReturn(emp);
		Assert.assertEquals(customerService.findById(emp.getId()),emp);
	}

	@Test
	public void saveCustomer(){
		doNothing().when(dao).saveCustomer(any(Customer.class));
		customerService.saveCustomer(any(Customer.class));
		verify(dao, atLeastOnce()).saveCustomer(any(Customer.class));
	}
	
	@Test
	public void updateCustomer(){
		Customer customer = customers.get(0);
		when(dao.findById(anyInt())).thenReturn(customer);
		customerService.updateCustomer(customer);
		verify(dao, atLeastOnce()).findById(anyInt());
	}

	@Test
	public void deleteCustomerById(){
		doNothing().when(dao).deleteCustomerById(anyInt());
		customerService.deleteCustomerById(anyInt());
		verify(dao, atLeastOnce()).deleteCustomerById(anyInt());
	}
	
	@Test
	public void getAll(){
		when(dao.getAll()).thenReturn(customers);
		Assert.assertEquals(customerService.getAll(), customers);
	}

	@Test
	public void isCustomerUnique(){
		Customer customer = customers.get(0);
		when(dao.findById(anyInt())).thenReturn(customer);
		Assert.assertEquals(customerService.isCustomerUnique(customer.getLogin(), customer.getId()), true);
	}
	
	
	public List<Customer> getCustomerList(){
		Customer c1 = new Customer();
		c1.setFirstName("Cass");
		c1.setLastName("Los");
		c1.setLogin("Cass");
		c1.setPassword("ssss");
		c1.setId(1);
		
		Customer c2 = new Customer();
		c2.setFirstName("Dean");
		c2.setLastName("Los");
		c2.setLogin("Dean");
		c2.setPassword("ssss");
		c2.setId(2);
		
		customers.add(c1);
		customers.add(c2);
		return customers;
	}
	
}
